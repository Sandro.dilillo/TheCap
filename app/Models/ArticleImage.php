<?php

namespace App\Models;

use App\Models\Article;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ArticleImage extends Model
{
    use HasFactory;
    protected $fillable = [
       'name',
       'article_id',
       'image_path'
    ];

    public function article(){
        return $this->belongsTo(Article::class);
    }
}
