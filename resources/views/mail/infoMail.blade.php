<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
</head>
<body>
    <div style="background-color:#5497B5; padding: 5%; font-family: 'Poppins', sans-serif;">
        <h1 style="color:#e08c1e;  font-size: 24px;  text-align: center; font-family: 'Poppins', sans-serif;">Capperk.it</h1>
        <h2 style="color: #C59E7C; font-size: 24px;  text-align: center; font-family: 'Poppins', sans-serif;padding: 5%;">
           Grazie per averci contattato <span style="color: #ffbc42;">{{$contact['user']}}</span>
        </h2>
        <h4 style="color: #e08c1e; font-size: 18px; font-family: 'Poppins', sans-serif;padding-top: 5%;">
            Il tuo messaggio:
        </h4>
        <p style="color: #C59E7C; font-size: 16px; font-family: 'Poppins', sans-serif;padding-bottom: 5%;">
            {{$contact['message']}}
        </p>
        <h4 style="color: #e08c1e; font-size: 18px; font-family: 'Poppins', sans-serif;padding-top: 5%;">
           Articolo di riferimenti:
        </h4>
        <p style="color: #C59E7C; font-size: 16px; font-family: 'Poppins', sans-serif;padding-bottom: 5%;">
            {{-- {{$contact['article']}} --}}
        </p>
        <hr style="background-color: #C59E7C; color: #C59E7C;">
        <div style="display:flex; flex-wrap: wrap; justify-content: center; text-align: center;">                               
            <div style="color:#e08c1e;">
                <p>© 2021 Capperk.it</p>
                <p>Privacy Policy / Cookie policy / P.IVA 00000000000 | Design e sviluppo: lol</p>
                <p>- Copiright®</p>
            </div>
        </div>
    </div>
</body>
</html>
