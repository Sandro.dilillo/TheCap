<x-layout>
    <x-navbar />
       
                <div class="container bg-pattern1">
                    @if ($errors->any())
                        <div class="alert alert-danger my-5">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    <div class="row justify-content-evenly mt-3 border-custom3">
                        <div class="col-12 text-center mt-5 tc-sec ">
                            <h2 class="">Modifica articolo</h2>
                        </div>
                        
                        
                        <div class="col-12 col-md-5 my-4 ">
                           
                            <form method="POST" action="{{ route('article.update', compact('article')) }}" enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="mb-3 text-center">
                                    <label for="title" class="form-label tc-sec">Titolo</label>
                                    <input name="title" type="text" class="form-control rounded-pill" id="title" value="{{ $article->title }}">
                                </div>
                                <div class="mb-3 text-center">
                                    <label for="price" class="form-label tc-sec">Prezzo</label>
                                    <input name="price" type="number" class="form-control rounded-pill" id="price" value="{{ $article->price }}">
                                </div>                        
                                <div class="mb-3 text-center">
                                    <label for="description" class="form-label tc-sec">Aggiungi descrizione</label>
                                    <textarea name="description" id="description" class="form-control rounded" cols="30"
                                        rows="5">{{ $article->description }}</textarea>
                                </div>
                               
                                <div class="form-group">
                                    <label for="title">Aggiungi immagine</label>
                                    <input type="file" name="images[]" class="form-control" multiple="">
                                    @if($errors->has('images'))
                                    <span class="help-block text-danger">{{ $errors->first('images') }}</span>
                                    @endif
                                </div>
                               
                                <div class="d-flex col-12 justify-content-between mt-3">


                                    <div class=" col-4 text-center">
                                        <a href="{{ route('article.show', compact('article')) }}"
                                        class="btn btn-custom3">
                                        Torna indietro
                                        </a>
                                    </div>


                                    <div class="col-4 text-center">
                                        <button type="submit" class="btn btn-custom2 underline-cstm">
                                            Modifica 
                                        </button>
                                    </div>


                                    


                                </div>
            
                               
                            </form>
                        </div>
                        <div class="col-4 d-flex  justify-content-centercol-md-5 mb-2">
                            @foreach ($articleImages as $articleImage)
                           
                            @if ($article->id == $articleImage->article_id )
                            <img src="{{ asset($articleImage->image_path) }}" class="card-img-top img-fluid rounded shadow">
                            <div class="x">
                                <form method="POST" action="{{ route('article.removeImage', compact('articleImage')) }}">
                                    @csrf
                                    @method('delete')
                                    <button class="btn-close btn-close-bg" type="submit" aria-label="Close"></button>
                                </form>
                            </div>
                            @endif
                          
                            @endforeach
                        </div>
                    </div>
                </div>
            
    
    <x-footer />
</x-layout>