<x-layout>
    <x-navbar />
    <div class="container" style="margin-bottom: 150px">
        <div class="row justify-content-evenly my-3 border-custom2">
            <div class="col-12 text-center mt-5 mb-2 tc-accent">
                <h1 class="fw-bold">Dettaglio</h1>
            </div>
            <div class="col-12">
                <a href="{{ route('article.index') }}"><i class="fas fa-chevron-left tc-accent fs-2"></i></a>
            </div>
            <div class="col-12 col-md-5"> 
            <div class="slider-for">
                @foreach ($articleImages as $articleImage)

                    @if ($article->id == $articleImage->article_id)
                        <img src="{{ asset($articleImage->image_path) }}"
                            class="card-img-top img-fluid rounded shadow ">
                    @endif

                @endforeach
            </div>
            <div class="slider-nav">
                @foreach ($articleImages as $articleImage)

                    @if ($article->id == $articleImage->article_id)
                        <img src="{{ asset($articleImage->image_path) }}"
                            class="card-img-top img-fluid rounded shadow mb-4">
                    @endif

                @endforeach
            </div>
        </div>
            <div class="col-12 d-flex flex-column col-md-5 mb-5 text-center justify-content-between">
                <div>
                    <h2 class="card-title tc-sec">{{ $article->title }}</h2>
                    <p class="card-text tc-main fw-bold">{{ ucfirst($article->category->name) }}</p>
                    <p class="card-text tc-main fw-bold">{{ $article->price }} €</p>
                    <p class="card-text tc-main text-truncate">{{ $article->description }} 
                    </p>
                    <h6 class="card-text tc-accent">{{ $article->user->name }}</h6>
                    <p class="tc-accent ">{{ $article->created_at->format('d/m/y') }}</p>
                </div>
                @if (Auth::id() == $article->user->id)
                    <div class="col-12 d-flex justify-content-evenly">
                        <a href="{{ route('article.edit', compact('article')) }}"
                            class="btn btn-custom3 underline-cstm">
                            Modifica
                        </a>

                        <form method="POST" action="{{ route('article.destroy', compact('article')) }}">
                            @csrf
                            @method('delete')
                            <button class="btn btn-custom4 tc-base2 underline-cstm" type="submit">
                                Cancella
                            </button>
                        </form>
                    </div>
                @else
                    <div class="col-12 d-flex justify-content-evenly">
                        <a href="{{ route('richiesta_info', compact('article')) }}"
                            class="btn btn-custom2 underline-cstm">Richiedi info</a>
                    </div>
                @endif

            </div>
        </div>
    </div>
    </div>

    <x-footer />
    @push('lowscript')
        <script>
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav'
            });
            $('.slider-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: true,
                centerMode: true,
                focusOnSelect: true
            });

        </script>
    @endpush
</x-layout>
