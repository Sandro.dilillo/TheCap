<?php

namespace App\Jobs;

use Spatie\Image\Image;
use Illuminate\Bus\Queueable;
use Spatie\Image\Manipulations;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ResizeImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $path, $fileName, $id, $w, $h;


    public function __construct($filePath, $w, $h)
    {
        $this->path = dirname($filePath);
        $this->fileName = basename($filePath);
        // $this->id=$id;
        $this->w=$w;
        $this->h=$h;
    }
    public function handle()
    {
        $w = $this->w;
        $h = $this->h;
        $srcPath = public_path() . "/storage/".  $this->path . "/". $this->fileName;
        dump($srcPath);
        
        $destPath = public_path() . "/storage/".  $this->path . "/crop{$w}x{$h}_" . $this->fileName ;
        
        Image::load($srcPath)
            ->crop(Manipulations::CROP_CENTER, $w, $h)
            ->save($destPath);
    }

}
