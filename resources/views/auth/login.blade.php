<x-layout>
    <x-navbar />
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="container-fluid width-auth pt-3 pt-sm-5">
            <div class="row w-100 justify-content-center py-3 mx-0 border-custom2">
                <div class="container ">
                    <div class="row">
                      <div class="col-12">
                        <h2 class="tc-sec text-center ">  Accedi</h2>
                      </div>
                    </div>
                  </div>
                <div class="col-12 col-md-8 border-custom bg-base text-center">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group py-2">
                            <label for="exampleInputEmail1" class="">Indirizzo Email</label>
                            <input type="email" class="form-control rounded-pill" name="email" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Enter email" value="{{ old('email') }}">
                        </div>
                        <div class="form-group py-2">
                            <label for="exampleInputPassword1" class="">Password</label>
                            <input type="password" class="form-control rounded-pill" name="password" id="exampleInputPassword1"
                                placeholder="Password" >
                        </div>
                        <button type="submit" class="btn btn-custom3 tc-sec underline-cstm pt-2">Accedi</button>
                        <div class="modal-footer">
                            <p class="tc-sec">Non sei ancora registrato?</p>
                            <a href="{{ route('register') }}" type="button"
                            class="btn btn-custom2 me-2 tc-sec active underline-cstm">
                           Registrati
                        </a> 
            
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <x-footer />
</x-layout>
