<?php
return[

//home footer e navbar
'welcome'=> 'Benvenuto',
'i nostri cappelli'=>'I nostri cappelli',
'annunci'=>'Annunci!',
'sei nuovo qui'=>'sei nuovo qui?',
'nuove creazioni' => 'Nuove creazioni',
'bottone email'=>'Entra nella tana',
'titolo footer'=>'Segui il coniglio nella sua tana!',
'descrizione footer'=>"Nella tana del coniglio troverai tantissimi altri utenti, l'obiettivo del nostro sito è collegare tutto il mondo e rendere la vostra avventura veloce come il bianconiglio!",
'contatti'=>'Contatti',
'chi siamo'=>'Chi siamo',
'aiuto'=>'Aiuto',
'vendi'=>'Vendi un oggetto',
'dettaglio'=>'Dettaglio',
'segui'=>'Segui il coniglio!',
'search'=>'Cosa stai cercando...',
'cestino'=>'Cestino',
'ciao'=>'Ciao',
'Accedi||iscriviti'=>'Accedi || Iscriviti',
'Accedi'=>'Accedi',
'registrati'=>'Registrati',
'logout'=>'Esci',
'revisore'=>'Revisore',
'Le nostre'=>'Le nostre',
'categorie'=>'Categorie',
'cappelli da adulti' => 'Cappelli da adulti',
'cappelli da bimbi' => 'Cappelli da bimbi',
'vai'=>'Vai',
'conferma'=>'Conferma',

//modale
'Indirizzo Email'=>'Indirizzo Email',
'non sei ancora'=>"Non sei ancora registrato?",
'nome utente'=>'Nome Utente',
'conferma'=>'Conferma Password',
'nome e cognome'=>'Nome e Cognome',
'iscriviti'=>'Iscriviti',


//messaggio
'creato con succ'=>'Annuncio creato con successo!',
'solo rev'=>'Accesso negato, solo revisori',

//article/create
'inserisci'=>'Inserisci',
'il tuo annuncio'=>'il tuo annuncio!',
'nome articolo'=>'Nome articolo',
'scegli'=>'Scegli una categoria:',
'descrizione'=>'Descrizione',
'prezzo'=>'Prezzo',
'contatto'=>'Contatti',
'inserisci-i'=>'Inserisci immagini',
'aggiungi articolo'=>'Aggiungi articolo',

//dettaglio
'creato il'=>'Creato il',
'da'=>'da',
'contattami qui'=>'contattami qui :',
'torna alla home'=>'torna alla home',

//Revisor home
'titolo'=>'Non ci sono Annunci da revisionare ',
'sotto-titolo'=>'Beviti un caffe!',
'annuncio nr'=>'Annuncio Nr',

//revisor trash
'titolo-trash'=>'Non ci sono annunci nel tuo cestino ',
'sotto-trash'=>'Troppo caffè fa male!',
'cancella'=>'Cancella',

//lavora con noi
'lavora con'=>'Lavora con',
'noi!'=>'Noi!',
];
