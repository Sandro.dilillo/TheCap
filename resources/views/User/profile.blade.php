<x-layout>
    <x-navbar />

    <div class="container d-flex justify-content-center">
        <div class="col-12 border-custom2 mt-5 px-4 pt-4 pb-2 rounded-3 d-flex flex-column justify-content-center">
                <div class="text-center">
                    <div class=" p-2"> <img src="https://picsum.photos/200" class="mr-1 align-self-start">
                    </div>
                </div>
                <div class="text-center">
                    <div class="d-flex flex-row justify-content-center">
                        <h6 class="mt-2 mb-0 tc-sec text-center ">{{Auth::user()->name}}</h6><i class="fas fa-angle-down mr-3 "></i>
                    </div>
                    <p class="text-muted">Los Angeles</p>
                    <p class="text-muted">{{Auth::user()->email}}</p>
                    <p class="text-muted">Registrato da {{Auth::user()->created_at}}</p>
                </div>
                
            <ul class="list text-muted mt-3 pl-0 d-flex justify-content-around">
                <li><i class="far fa-building mr-3 ml-2"></i>Bussiness account</li>
                <li><i class="fas fa-wallet mr-3 ml-2"></i>Finance management</li>
                <li><i class="far fa-credit-card mr-3 ml-2"></i>Transactions</li>
                <li><i class="fas fa-chart-pie mr-3 ml-2"></i>Spending analysis</li>
            </ul>
        </div>
        </div>
    </div>

    <x-footer />

</x-layout>
