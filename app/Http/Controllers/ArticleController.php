<?php

namespace App\Http\Controllers;

// use App\Models\Image;
use App\Models\Cart;
use App\Models\Article;
use Spatie\Image\Image;
use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\ArticleImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('index', 'show', 'addToCart', 'cart');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        $articleImages = ArticleImage::all();
        $articles = Article::orderBy('created_at', 'desc')->get();
        return view('article.index', compact('articles', 'articleImages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('article.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $validator = \Validator::make($request->all(), [
            'title' => 'required',
            'images' => 'required',
            'price' => 'required',
            'description' => 'required'
        ])->validate();
      
        $article = new Article();
        $article->title = $request->title;
        $article->price = $request->price;
        $article->description = $request->description;
        $article->user_id = Auth::id();
        $article->category_id = $request->category;
        $article->save();
        if ($request->hasfile('images')) {
            $images = $request->file('images');

            foreach($images as $image) {
                $name = $image->getClientOriginalName();
                $path = $image->storeAs('uploads/'.$article->id, $name, 'public');
               
                dispatch(new ResizeImage(
                    $path,
                     300,
                     150
                   ));
                $articleImage = ArticleImage::create([
                    'name' => $name,
                    'article_id' => $article->id,
                    'image_path' => '/storage/'.$path
                    ]);
                    
                   
            }
            
        }

     return redirect(route('article.index'))->with('status', 'Grande! Hai appena inserito una nuova creazione');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article, ArticleImage $articleImage)
    {
        $articleImages = ArticleImage::all();
        
        return view('article.show', compact('article', 'articleImages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {   
        $articleImages = ArticleImage::all();
        return view('article.edit', compact('article','articleImages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, Article $article, ArticleImage $articleImage)
    {   
        $validator = \Validator::make($request->all(), [
            'title' => 'required',
            // 'images' => 'required',
            'price' => 'required',
            'description' => 'required'
        ])->validate();

        $article->title = $request->title;
        $article->price = $request->price;
        $article->description = $request->description;
        // $article->category_id = $request->category;
        $article->save();
        if ($request->hasfile('images')) {
            $images = $request->file('images');

            foreach($images as $image) {
                $name = $image->getClientOriginalName();
                $path = $image->storeAs('uploads/'.$article->id, $name, 'public');
                dispatch(new ResizeImage(
                    $path,
                     300,
                     150
                   ));
                ArticleImage::create([
                    'name' => $name,
                    'article_id' => $article->id,
                    'image_path' => '/storage/'.$path
                  ]);
            }
            
        }
       
        
        // return redirect()->back();  
        return redirect(route('article.index'))->with('status', 'Grazie , il tuo articolo è stato modificato!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article, ArticleImage $articleImage)
    {
        $articleImages = ArticleImage::all();
     
        foreach ($articleImages as $articleImage) {
            if ($articleImage->article_id == $article->id) {
            $name = $articleImage->name;
            $path = $articleImage->image_path;
            
            // Session::forget($path);
            ArticleImage::where("id", $articleImage->id)->delete();
            }
        }
        $article->delete();
        return redirect(route('article.index'))->with('status', 'Hai appena cancellato un articolo!');

    }

   
        public function removeImage(Request $request, ArticleImage $articleImage)
	{       
            
        
        
            // dd($articleImage);
            $articleRemove = ArticleImage::where("id", $articleImage->id)->get()->first();
            // dd($articleRemove);
            $articleRemove->delete();             
            
            return redirect()->back();
	}


    
    // CART
    
    public function addToCart(Request $request, $id) 
    {
        $article = Article::find($id);
        $article = json_decode(json_encode($article));
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($article, $article->id);
        
        $request->session()->put('cart', $cart);
        // dd($request->session()->get('cart'));
        return redirect()->route('article.index');
    }
    
    public function cart() 
    {
        $articleImages = ArticleImage::all();
        if (!Session::has('cart')) 
        {
            return view('article.shoppingCart', ['articles' => null]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $articles = $cart->items;
        $totalPrice = $cart->totalPrice;
       
        // dd($cart);
        return view('article.shoppingCart', compact('articles', 'totalPrice', 'articleImages'));
    }

    public function updateCart(Request $request, $id)
{
    
    // $cart = Session::get('cart');
    $article = Article::find($id);
    $article = json_decode(json_encode($article));
    $oldCart = Session::has('cart') ? Session::get('cart') : null;
    $cart = new Cart($oldCart);
    $cart->add($article, $article->id);
    $request->session()->put('cart', $cart);
    return redirect()->back();
}

public function deleteOneCart(Request $request, $id) 
{
    $article = Article::find($id);
    $article = json_decode(json_encode($article));
    $oldCart = Session::has('cart') ? Session::get('cart') : null;
    $cart = new Cart($oldCart);
    $cart->minusOne($article, $article->id);
    $request->session()->put('cart', $cart);
    return redirect()->back();
}
public function removeFromCart(Request $request, $id) 
{   
    
 

    $article = Article::find($id);
    $article = json_decode(json_encode($article));
    $oldCart = Session::has('cart') ? Session::get('cart') : null;
    $cart = new Cart($oldCart);
    $cart->remove($article, $article->id);
    $request->session()->put('cart', $cart);
    return redirect()->back();
}

public function checkout()
 {
    if (!Session::has('cart')) 
        {
            return view('article.shoppingCart', ['articles' => null]);
        }
    $oldCart = Session::get('cart');
    $cart = new Cart($oldCart);
    $total = $cart->totalPrice;
    // dd($cart);
    return view('article.shoppingCart.checkout', compact('total'));
 }


}





