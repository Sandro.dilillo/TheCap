<x-layout>
    <x-navbar />
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container-fluid width-auth pt-2 pt-sm-5">
        <div class="row w-100 justify-content-center py-3 mx-0 border-custom2">
            <div class="container ">
                <div class="row">
                  <div class="col-12">
                    <h2 class="tc-accent text-center"> Registrati</h2>
                  </div>
                </div>
              </div>
            <div class="col-12 col-md-8 border-custom bg-base text-center">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group pt-1">
                        <label for="name" class="my-1">Nome Utente</label>
                        <input type="text" class="form-control rounded-pill" name="name" id="name" placeholder="Nome e Cognome" value="{{ old('name') }}">
                    </div>
                    <div class="form-group pt-1">
                        <label for="exampleInputEmail1" class="my-1">Indirizzo Email</label>
                        <input type="email" class="form-control rounded-pill" name="email" id="exampleInputEmail1"
                            aria-describedby="emailHelp" placeholder="Email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group pt-1">
                        <label for="exampleInputPassword1" class="my-1">Password</label>
                        <input type="password" class="form-control rounded-pill" name="password" id="exampleInputPassword1"
                            placeholder="Password">
                    </div>
                    <div class="form-group pt-1">
                        <label for="password_confirmation" class="my-1">Conferma Password</label>
                        <input type="password" class="form-control rounded-pill" name="password_confirmation"
                            id="password_confirmation" placeholder="Conferma password">
                    </div>
                    <button type="submit" class="btn btn-custom2 tc-sec underline-cstm mt-2">Registrati</button>
                </form>
            </div>
        </div>
    </div>
      
    <x-footer />
</x-layout>
