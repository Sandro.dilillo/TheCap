{{ $slot }}
<div class="container-fluid pt-3">


    <div class="row">


        <div class="col-12 d-flex justify-content-center">
            <div class="col-10">
                <div class="footer-basic bg-base rounded-pill mt-0 mt-md-3 ">
                    <footer>
                        <div class="col-12 d-flex flex-column align-items-center ">
                            <div class="col-12 social mt-3">
                                <a href="#"><i class="fab fa-instagram social"></i></a>
                                <a href="#"><i class="fab fa-linkedin social"></i></a>
                                <a href="#"><i class="fab fa-twitter social"></i></a>
                                <a href="#"><i class="fab fa-facebook social"></i></a>
                            </div>
                            <div class="col-10 text-center d-flex justify-content-center social me-1 pb-2">
                                <p class="tc-base copyright">Company Name © 2018</p>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </div>
</div>
