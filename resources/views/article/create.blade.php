<x-layout>
    <x-navbar />
    <div class="container bg-pattern1">
        @if ($errors->any())
            <div class="alert alert-danger my-5">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
         @endif
        <div class="row justify-content-evenly mt-3 border-custom3">
            <div class="col-12 text-center mt-5 tc-sec ">
                <h2 class="">Inserisci la tua nuova creazione</h2>
            </div>
            
            <div class="col-12 col-md-5 my-4 ">
                <form method="POST" action="{{ route('article.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3 text-center">
                        {{-- <label for="title" class="form-label tc-main">Titolo</label> --}}
                        <input name="title" type="text" class="form-control rounded-pill" id="title"
                            value="{{ old('title') }}" placeholder="Inserisci qui il nome del tuo cappello">
                    </div>
                    <div class="mb-3 text-center">
                        <label for="" class="form-label tc-sec">Scegli una categoria</label>
                        <select name="category" class="tc-sec custom-select my-select border mb-1">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ ucfirst($category->name) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-3 text-center">
                        {{-- <label for="price" class="form-label tc-main">Prezzo</label> --}}
                        <input name="price" type="number" class="form-control rounded-pill" id="price"
                            value="{{ old('price') }}" placeholder="Inserisci qui il prezzo">
                    </div>
                    <div class="mb-3 text-center">
                        {{-- <label for="description" class="form-label tc-main">Aggiungi descrizione</label> --}}
                        <textarea name="description" id="description" class="form-control rounded" cols="30"
                            rows="5" placeholder="Inserisci qui la descrizione dell'articolo">{{ old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">Image/file</label>
                        <input type="file" name="images[]" class="form-control" multiple="">
                        @if($errors->has('images'))
                        <span class="help-block text-danger">{{ $errors->first('images') }}</span>
                        @endif
                    </div>

{{-- 
                    <div class="my-3 text-center">
                            <label for="img" class="form-label tc-sec">Inserisci qui l'immagine</label>
                            <input type="file" name="img" class="form-control" id="img">
                        </div> --}}
                      
                    <div class="text-center my-3">
                        <button type="submit" class="btn btn-custom2 underline-cstm">Pubblica il tuo annuncio</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <x-footer />
</x-layout>
