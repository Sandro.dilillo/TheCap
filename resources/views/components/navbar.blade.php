<div class="col-12 d-flex justify-content-center pt-0">
    <div class="col-10">
        <div class="bg-nav text-center rounded-pill border pt-0">


            <div>
                <a class="" href="{{ route('home') }}"><img src="/img/cappelloLogo.png" class="mx-auto logo-nav pt-0 " alt="Capperk.it">
                    <h2 class="tc-main pb-0 mb-0">The Capperk</h2>
                </a>
                
            </div>
            <div class="col-12 d-none d-sm-flex justify-content-center ">
                <div class="col-8 d-flex justify-content-between">
                
                    <li class="nav-item dropdown d-flex justify-content-end ">
                        <button class="btn btn-custom2 dropdown-toggle" type="button" id="dropdownMenuButton1"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            {{ ucfirst(app()->getLocale()) }}
                            
                        </button>
                        @if (app()->getLocale() == "it")
                                <span class="flag-it px-3"></span>
                                @elseif(app()->getLocale() == "en")
                                <span class="flag-en px-3"></span>
                                @elseif(app()->getLocale() == "es")
                                <span class="flag-es px-3"></span>
                                
                        @endif
                        <ul class="dropdown-menu " aria-labelledby="dropdownMenuButton1">
                            <li class="flag-it"><a class="dropdown-item " href=""> @include('components.locale',
                                    ['lang'=>'it','nation'=>'IT'])</a></li></span>
                            <li class="flag-en "><a class="dropdown-item" href="#"> @include('components.locale',
                                    ['lang'=>'en','nation'=>'GB'])</a></li>
                            <li class="flag-es "><a class="dropdown-item" href="#"> @include('components.locale',
                                    ['lang'=>'es','nation'=>'ES'])</a></li>
                        </ul>
                    </li>
                        @guest
                         <div class="text-end">
                        <button type="button" class="btn btn-custom5 tc-sec active" data-bs-toggle="modal"
                        data-bs-target="#exampleModal">
                                
                            <i class="fas fa-search"style="border-radius: 100% "></i>
        
                        </button>
                        </div>
                        
                        @endguest
        
                    </div>
            </div>
        </div>
       
        <nav class="navbar navbar-expand-md bg-nav navbar-light shadow-nav rounded-3 mt-0 pt-0 border">
            <div class="container-fluid ">
                <div class="mx-auto">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-hat-wizard tc-accent fs-2"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mb-2 mb-lg-0 mx-auto">
                        <li class="nav-item">
                            <a class="nav-link underline-cstm tc-main rounded-pill" aria-current="page"
                                href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link underline-cstm rounded-pill" href="{{ route('article.index') }}">{{ __('ui.i nostri cappelli') }}</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="categoryDropdown" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    {{ __('ui.categorie') }}<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="categoryDropdown">
                                    @foreach ($categories as $category)
                                        
                                    <li>
                                        <a class="dropdown-item" href="{{ route('category', ['cat' => $category->id])}}">
                                            {{$category->name}}
                                        </a>
                                    </li>

                                    @endforeach
                                   
                                 
                                </ul>
                        </li>
                        @guest

                            <li class="nav-item">
                                <a class="nav-link underline-cstm rounded-pill"
                                    href="{{ route('richiesta_info.home') }}"> {{ __('ui.contatti') }}</a>
                            </li>
                        @endguest
                        @auth

                        <li class="nav-item">
                            <a class="nav-link underline-cstm rounded-pill"
                                href="{{ route('richiesta_info.home') }}"> {{ __('ui.contatti') }}</a>
                        </li>

                        
                        @if(Auth::user()->is_admin)
                        <li class="nav-item">
                            <a class="nav-link underline-cstm" href="{{ route('article.create') }}"><span
                                class="tc-accent fw-bold"> {{ __('ui.nuove creazioni') }}</span>
                            </a>
                        </li>
                            
                        @endif
                       
                        <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" 
                                    href="#" id="navbarDropdown" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">

                                                                {{ __('ui.welcome') }}, 

                                    <span class="tc-accent fw-bold">

                                                                {{ Auth::user()->name }}

                                    </span>!
                                </a>

                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">

                                    <li>
                                        <a class="dropdown-item" href="{{ route('user.profile') }}">
                                        <i class="fas fa-user-circle tc-accent"></i>
                                            Profilo 
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault(); document.getElementById('form-logout').submit();"><i
                                                class="fas fa-sign-out-alt tc-accent"></i>{{ __('ui.logout') }}</a>
                                    </li>
                                    <form method="POST" action="{{ route('logout') }}" style="display: none"
                                        id="form-logout">
                                        @csrf
                                    </form>
                                </ul>

                                
                            @else
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    {{ __('ui.welcome') }},  {{ __('ui.sei nuovo qui') }} </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li class="">
                                        <a class="dropdown-item" href="{{ route('register') }}">{{ __('ui.registrati') }}<span></span><i
                                                class="fas fa-user-plus tc-accent ps-3"></i> 
                                        </a>
                                    </li>
                                    <li><a class="dropdown-item" href="{{ route('login') }}">{{ __('ui.Accedi') }}<span></span><i
                                                class="fas fa-user-check tc-accent ps-3"></i>
                                        </a>
                                    </li>
                                    {{-- <li><a class="dropdown-item" href="{{ route('login') }}">{{ __('ui.welcome') }}<span></span> <i
                                                class="fas fa-hand-spock tc-accent"></i> </a></li> --}}
                                </ul>
                            </li>
                            @endauth
                            <li class="nav-item">
                                <a class="nav-link " href="{{route('article.shoppingCart')}}">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="badge bg-sec"> {{Session::has('cart') ? Session::get('cart')->totalQty : ""}}</span>
                            </a>
                            </li>



                            
                        <li class="nav-item dropdown d-block d-sm-none justify-content-start">
                            <button class="btn btn-custom3 dropdown-toggle" type="button" id="dropdownMenuButton1"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                {{ ucfirst(app()->getLocale()) }}
                                
                            </button>
                            @if (app()->getLocale() == "it")
                                    <span class="flag-it p-3"></span>
                                    @elseif(app()->getLocale() == "en")
                                    <span class="flag-en p-3"></span>
                                    @elseif(app()->getLocale() == "es")
                                    <span class="flag-es p-3"></span>
                                    
                            @endif
                            <ul class="dropdown-menu " aria-labelledby="dropdownMenuButton1">
                                <li class="flag-it"><a class="dropdown-item " href=""> @include('components.locale',
                                        ['lang'=>'it','nation'=>'IT'])</a></li></span>
                                <li class="flag-en "><a class="dropdown-item" href="#"> @include('components.locale',
                                        ['lang'=>'en','nation'=>'GB'])</a></li>
                                <li class="flag-es "><a class="dropdown-item" href="#"> @include('components.locale',
                                        ['lang'=>'es','nation'=>'ES'])</a></li>
                            </ul>
                        </li>
                        </div>
    

                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>




{{-- modal --}}



<div class="modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header justify-content-center">
                <h5 class="modal-title tc-sec " id="exampleModalLabel">Cerca un cappello</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('search') }}" method="GET" class="d-flex my-2">
                    <input class="form-control me-2 cerca-custom" type="text"
                        placeholder="Cosa stai cercando.." aria-label="Search" name="q">
                    <button class="btn-custom4" type="submit"><i class="fas fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>

