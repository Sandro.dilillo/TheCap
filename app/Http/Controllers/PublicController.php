<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\InfoMail;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class PublicController extends Controller
{
    
    public function home() {
       $categories = Category::all();
       return view('welcome', compact('categories'));
     
    }


// contatti


    public function richiesta_info(Article $article) {
        
       return view('info', compact('article'));
     
    }
    public function richiesta_info2() {
        
       return view('info');
     
    }
    public function submit(Request $request) {
        $user = $request->input('user');
        $message = $request->input('message');
        $email = $request->input('email');
        $article = $request->input('article');
        $contact= compact('user','message');

        Mail::to($email)->send(new InfoMail($contact));
      
        return redirect(route('home'))->with('message','Il tuo messaggio è stato inoltrato correttamente, a breve riceverai una email di conferma');
    }

    //    return view('info', compact('article'));
     
    // ricerca 

    
    public function search(Request $request) {
        $q = $request->input('q');
        $articles = Article::search($q)-> query(function ($builder) { $builder->with(['category']); })->get();
        return view('search_results', compact('q', 'articles'));
    } 
    


//categorie indice

    public function category($cat){

        $category=Category::where('id', $cat)->pluck('name')->pop();

        $articles=Article::where('category_id', $cat)->get();
        

        return view('category', compact('articles', 'category'));
    }


    // bandierine lingua
    public function locale($locale) {
        Session::put('locale', $locale);
        return redirect()->back();
    }
}

