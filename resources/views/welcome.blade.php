<x-layout>
    <x-navbar />

    <!-- head -->
    <header class="masthead pt-3 pt-md-3">
        <div class="container-fluid d-flex justify-content-center">


            {{-- no carosello --}}
            <div class="row align-items-center border-custom2 bg-base2 pt-1 d-none d-sm-block">
                <div class="col-12 d-md-flex d-column justify-content-center justify-content-md-around">
                    @foreach ($categories as $category)
                        <div class="font-weight-light mb-4 mx-2 ">
                            <div class="card rounded ">
                                @if ($category->id == 1)

                                <img src="/img/oldHome.png"
                                    class="card-img-top rounded img-fluid" alt="cappelli da adulti immagine categoria">
                         
                                 @else
                                  
                                 <img src="/img/bimboHome.png"
                                 class="card-img-top rounded img-fluid" alt="cappelli da bimbi immagine categoria">
                 
                                @endif
                                <div class="card-body text-center">
                                    <h5 class="card-title mb-0 tc-sec">{{ $category->name }}</h5>
                                    <a href="{{ route('category', ['cat' => $category->id]) }}"
                                        class="btn btn-custom2 tc-sec underline-cstm mt-2 rounded-pill">La
                                        collezione
                                    </a>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>

                <div class="col-12 d-none d-sm-block">
                    <div class="col-12 text-center pb-4">
                        <a href="" class="btn btn-custom2 tc-sec underline-cstm mt-0 rounded-pill p-3"> Cappello su
                            misura?
                            clicca qui! </a>
                    </div>
                </div>
            </div>


            {{-- vista smatphone --}} {{-- si carosello --}}


            <div class="col-12 pt-2 d-block d-sm-none">
                
                <div class="row my-2" >
                  
                    <div class="col-12 slider-for">
                        @foreach ($categories as $category)
                            <div>

                                    <div class="card rounded my-4 mx-3">
                                        @if ($category->id == 1)

                                            <img src="/img/oldHome.png"
                                                class="card-img-top rounded img-fluid" alt="cappelli da adulti immagine categoria">
                         
                                        @else
                                  
                                            <img src="/img/bimboHome.png"
                                            class="card-img-top rounded img-fluid" alt="cappelli da bimbi immagine categoria">
                 
                                         @endif
                                        <div class="card-body text-center">
                                            <h5 class="card-title mb-0 tc-sec">{{ $category->name }}</h5>
                                            <a href="{{ route('category', ['cat' => $category->id]) }}"
                                                class="btn btn-custom2 tc-sec underline-cstm mt-2 rounded-pill">La
                                                collezione</a>
                                        </div>
                                    </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="pt-btn col-12 d-xs-block d-sm-none">
                        <div class="col-12 text-center pt-3">
                            <a href="" class="btn btn-custom2 tc-sec underline-cstm mt-0 rounded-pill p-3"> Cappello
                                su
                                misura?
                                clicca qui! </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        {{-- <div class="pt-1"></div> --}}














        {{-- <img class="img-fluid round-img" src="/img/shopping.jpg"> --}}
        {{-- @if ($article->img)
                <img src="{{ Storage::url($article->img) }}"class="card-img-top img-fluid rounded-circle" alt="">
                @else
                <img src="/img/200x100.png" class="card-img-top img-fluid rounded-circle" alt="">
                @endif --}}


                <x-footer />
    </header>


    @push('lowscript')
        <script>
            $('.slider-for').slick({
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 2,
                responsive: [{
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            });

        </script>
    @endpush


</x-layout>
