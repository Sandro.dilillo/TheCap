<x-layout>
    <x-navbar />
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        var paymentConfig = {
            publicKey: "{{ env('STRIPE_KEY') }}"
        };

    </script>


    <div class="container mt-5 px-5">
        <div class="row">
            <div class="col-md-8 border-custom2">


                <form id="payment-form" action="{{ route('stripe.payment') }}" method="post" >

                    <div id="card-element">
                        <div class="inputbox mt-3 ">
                            <input type="text" class="form-control" required="required">
                            <label class="tc-main">Name on card</label>
                        </div>
                        <div id="card-element">
                            <div class="inputbox mt-3 mr-2">
                                <input autocomplete='off' class='form-control card-num' size='20' type='text'
                                    required="required">
                                <i class="fa fa-credit-card"></i> <label class="tc-main">Card Number</label>
                            </div>
                        </div>
                        <div id="card-element">
                            <div class="inputbox mt-3 mx-2 ">
                                <input class='form-control card-expiry-month' placeholder='MM' size='2' type='text'
                                    required="required">
                                <label class="tc-main">Month</label>
                            </div>
                        </div>
                        <div id="card-element">
                            <div class="inputbox mt-3 mx-2 ">
                                <input class='form-control card-expiry-year' id="card-element" placeholder='YYYY'
                                    size='4' type='text' required="required">
                                <label class="tc-main">Year</label>
                            </div>

                        </div>
                        <div id="card-element">
                            <div class="inputbox mt-3 mr-2 cvc">
                                <input autocomplete='off' class='form-control card-cvc' id="card-element" placeholder=''
                                    size='4' type='text' required="required">
                                <label class="tc-main">CVV</label>
                            </div>

                        </div>

                    </div>



                    <div id="card-errors" role="alert">
                        <div class='form-row row ' >
                            <div class='col-md-12 hide error form-group d-none'>
                                <div class='alert-danger alert'>Fix the errors before you begin.</div>
                            </div>
                        </div>
                    </div>
                    <div id="card-success" role="alert">
                        <div class='form-row row ' id="card-errors">
                            <div class='col-md-12 hide error form-group d-none'>
                                <div class='alert-danger alert'>COmplimenti il pagamento è andato a buon fine</div>
                            </div>
                        </div>
                    </div>


                    <p>
                        <input type="hidden" name="total" id="total" value="{{ $total }}">
                        @csrf
                        <button type="submit" id="pay" class="btn btn-primary">Paga ora</button>
                    </p>
                </form>



            </div>
        </div>
    </div>
    <x-footer />

    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/payment.js') }}"></script>

    <script>
        var stripe = Stripe(paymentConfig.publicKey);
        var elements = stripe.elements({
            locale: 'it'
        });
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };
        var card = elements.create('card', {
            style: style
        });
        card.mount('#card-element');

        card.addEventListener('change', function(error) {
            var displayError = document.getElementById('card-errors');
            if (error) {
                displayError.textContent = error.message;
            } else {
                displayError.textContent = '';
            }
        });



        $("#payment-form").on("submit", function(e) {
            e.preventDefault();
            $.post("./stripe-checkout", $(this).serialize(), function(response) {
                stripe.confirmCardPayment(response.secret, {
                    payment_method: {
                        card: card
                    }
                }).then(function(result) {
                    var displayError = document.getElementById('card-errors');
                    var displaySuccess = document.getElementById('card-success');
                    if (result.error) {
                        displayError.textContent = error.message;
                    } else {
                        if (result.paymentIntent.status === "succeeded") {
                            displayError.textContent = success.message;
                        } else {
                            // Gestione errore 
                        }
                    }
                });
            });
        });

    </script>
    {{-- <script type="text/javascript" src="https://js.stripe.com/v2/"></script> --}}

    {{-- <script type="text/javascript">
        // Create an instance of the Stripe object with your publishable API key
        var stripe = Stripe("pk_test_51Iw57eK9raLUvqgLCf5zZmf18zBoQicJHoIDNsTmHUnAtyW00qJ1RiEpKxzrec2nmJwD73bI8oB1akCFzeYLeD4k001GOC4rV9");
        var checkoutButton = document.getElementById("checkout-button");
        checkoutButton.addEventListener("click", function () {
          fetch("/StripeController.php", {
            method: "POST",
            .then(function (response) {
              return response.json();
            })
            .then(function (session) {
              return stripe.redirectToCheckout({ sessionId: session.id });
            })
            .then(function (result) {
              // If redirectToCheckout fails due to a browser or network
              // error, you should display the localized error message to your
              // customer using error.message.
              if (result.error) {
                alert(result.error.message);
              }
            })
            .catch(function (error) {
              console.error("Error:", error);
            });
        });
      </script> --}}

    </html>
</x-layout>
