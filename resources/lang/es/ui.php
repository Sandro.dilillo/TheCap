<?php
return[

//home footer e navbar
'welcome'=> 'Bienvenido',
'i nostri cappelli'=>'Nuestros sombreros',
'annunci'=>'Anuncios!',
'sei nuovo qui'=>'ya nos hemos conocido?',
'nuove creazioni' => 'Nuevas creaciones',
'bottone email'=>'Ingresar',
'titolo footer'=>'¡SIGUE AL CONEJO EN SU GUARDIA!',
'descrizione footer'=>'En la madriguera del conejo encontrarás muchos otros usuarios, ¡el objetivo de nuestro sitio es conectar al mundo entero y hacer que tu aventura sea tan rápida como el conejo blanco!',
'contatti'=>'Contactos',
'chi siamo'=>'Sobre nosotros',
'aiuto'=>'Ayudar',
'vendi'=>'Vende ahora',
'dettaglio'=>'Detalles',
'segui'=>'siga el conejo',
'search'=>'Buscar artículos ...',
'cestino'=>'Basura',
'ciao'=>'Hola',
'Accedi||iscriviti'=>'Iniciar || Subscribir',
'Accedi'=>'Iniciar',
'registrati'=>'Subscribir',
'logout'=>'Cerrar',
'revisore'=>'Revisor',
'Le nostre'=>'Nuestras',
'categorie'=>'Categorias',
'cappelli da adulti' => 'Sombreros para adultos',
'cappelli da bimbi' => 'sombreros para niños',
'vai'=>'Ir',

//modale
'Indirizzo Email'=>'Email Address',
'non sei ancora'=>"¿No se ha registrado?",
'nome utente'=>'Nombre Usuario',
'conferma'=>'Confirmar Password',
'nome e cognome'=>'Nombre y Apellido',
'iscriviti'=>'Suscribir',
'conferma'=>'Confirmar',

//messaggio
'creato con succ'=>'Anuncio creado correctamente!',
'solo rev'=>'Aceso denegado, solo para revisores!',

//article/create
'inserisci'=>'Para Insertar',
'il tuo annuncio'=>'tus articulos!',
'nome articolo'=>'Nombre Articulo',
'scegli'=>'Elige una categoria',
'descrizione'=>'Descripción',
'prezzo'=>'Precio',
'contatto'=>'Contacto',
'inserisci-i'=>'Insertar imagen',
'aggiungi articolo'=>'Insertat articulo',

//dettaglio
'creato il'=>'Creado en',
'da'=>'por',
'contattami qui'=>'contactame aqui :',
'torna alla home'=>'ir a la home',

//Revisor home
'titolo'=>'No hay anuncios para revisar',
'sotto-titolo'=>'¡Tomar un café!', 
'annuncio nr'=>'Anuncio Nr',

//revisor trash
'titolo-trash'=>'No hay anuncios en su cesta.',
'sotto-trash'=>'¡Demasiado café es malo!',
'cancella'=>'Cancelar',

//lavora con noi
'lavora con'=>'Trabaja con',
'noi!'=>'Nosotros!',

];
