<x-layout>
    <x-navbar />



    @if (Session::has('cart') == 1)
        <div class="container mt-5 mb-5">
            <div class="d-flex justify-content-center row ">
                <div class="col-md-8 border-custom2">
                    <div class="p-2 d-flex justify-content-between align-items-center ">
                        <h4 class="tc-sec fs-2">Il tuo carrello </h4>
                        <div class="p-2 ">
                            {{-- <div class="row"> --}}
                            <div class="col-12 d-flex justify-content-center pt-2">
                                {{-- @php(dd($totalPrice)) --}}
                                <h2 class="tc-sec fs-3  border-custom2 p-3"> Total: {{ $totalPrice }}</h2>
                            </div>
                            {{-- </div> --}}
                        </div>
                    </div>
                    <div class="d-flex flex-row align-items-center justify-content-end"><span class="mr-1 tc-sec">Ordina
                            per:</span><span class="mr-1 font-weight-bold tc-sec">Prezzo</span><i
                            class="fa fa-angle-down tc-sec "></i></div>
                    @foreach ($articles as $article)

                        <div
                            class="d-flex flex-row justify-content-between align-items-center p-2 bg-white mt-4 px-3 rounded">
                            @foreach ($articleImages as $articleImage)

                                <div class="mr-1">
                                    <img src="{{ $articleImage }}" class="rounded" width="70">
                                </div>
                            @break
                    @endforeach
                    <div class="d-flex flex-column align-items-center "><span
                            class="font-weight-bold tc-sec">{{ $article['item']->title }}</span>
                        <div class="d-flex flex-column product-desc">
                            {{-- @php(dd($article['item'])) --}}
                            <div class="size mr-1"><span
                                    class="text-grey  "> -----descrizione piccola ----- </span></div>
                                    <div><span
                                    class="font-weight-bold tc-sec">&nbsp;M</span></div>
                            <div class="color"><span class="text-grey">Color:</span><span class="font-weight-bold tc-sec">&nbsp;Grey</span></div>
                        </div>
                    </div>
                    <div class="d-flex flex-row align-items-center qty">

                        {{-- meno uno --}}


                        <form method="POST"
                            action="{{ route('article.deleteOneCart', ['id' => $article['item']->id]) }}"
                            enctype="multipart/form-data">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn-custom5"><i class="fa fa-minus text-danger mb-3"> </i></button>
                        </form>


                        {{-- quantità --}}


                        <h5 class="text-grey mt-1 mr-1 ml-1">{{ $article['qty'] }}</h5>


                        {{-- più uno --}}


                        <form method="POST" action="{{ route('article.updateCart', ['id' => $article['item']->id]) }}"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <button type="submit" class=" btn-custom5"><i class="fa fa-plus text-success mb-3"> </i></button>
                        </form>
                    </div>
                    <div>
                        <h5 class="text-grey">{{ $article['item']->price }} €</h5>
                    </div>
                    <div class="d-flex align-items-center">
                        <form method="POST" action="{{ route('article.removeFromCart', ['id' => $article['item']->id]) }}"
                            enctype="multipart/form-data">
                            @csrf
                            @method('delete')
                            <button type="submit" class=" btn-custom5"><i class="fa fa-trash mb-1 text-danger"></i></button>
                        </form> 
                    </div>
                </div>

    @endforeach
                <div class="d-flex  align-items-center mt-3 p-2 bg-white rounded">
                    <input type="text"
                        class="form-control border-0 gift-card" placeholder="discount code/gift card">
                        <button class="btn btn-outline-warning btn-sm ml-2" type="button">
                            Apply
                        </button>
                </div>
                <hr>
                <div class="row ">
                    <div class="col-12 d-flex justify-content-end pe-4 pb-3">
                    <a href="{{ route('article.shoppingCart.checkout') }}" class="btn btn-custom4 p-2 fw-bold fs-5"> 
                        Checkout
                    </a>
                </div>
            </div>
        </div>
    </div>
    

@else
    <div class="row ">
        <div class="col-12 text-center pt-5">
            <h1 class="tc-sec fs-3">Non hai articoli nel tuo carrello </h1>
        </div>
    </div>
    @endif

    </div>
    <x-footer />
</x-layout>
