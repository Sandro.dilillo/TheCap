<x-layout>
    <x-navbar />
    <div class="container my-5">
        <div class="row">
            <div class="col-12 category-border">
                <h2 class="text-center tc-sec pt-4 mt-2">{{ucfirst("Hai cercato".": ". $q)}}</h2>
            </div>
            @foreach ($articles as $article)
            <div class="col-12 col-md-4 p-3 mb-4">
                <div class="card rounded mt-2">
                    @if ($article->articleImages()->exists())
                    <img src="{{ asset($article->articleImages()->get()->first()->image_path) }}"
                    class="card-img-top img-fluid rounded w-auto" style="height: 300px">
                    
                    @else
                        <figure class="d-flex justify-content-center align-items-center">
                            <img src="https://via.placeholder.com/150" class="card-img-top img-fluid"
                                alt="placeholder">
                        </figure>
                    @endif
                    <div class="card-body text-center d-flex flex-column justify-content-between">
                        <div>
                            <h5 class="card-title tc-sec">{{ $article->title }}</h5>
                            <p class="card-text tc-sec">{{ $article->price }} €</p>
                            <p class="card-text tc-sec">{{ $article->category->name }} </p>
                            <p class="card-text tc-sec text-truncate">{{ $article->description }}</p>
                        </div>
                        <div>
                            <a href="{{ route('article.show', compact('article')) }}"
                                class="btn btn-custom2 underline-cstm">
                                Dettagli
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
            <div class="row justify-content-center">
            </div>
        </div>
    </div>

    <x-footer />
</x-layout>