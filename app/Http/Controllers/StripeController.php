<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Stripe;
use Session;

class StripeController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('handlePost');
    }
    /**
     * payment view
     */
    public function handleGet()
    {
        return view('home');
    }
  


    function calculateOrderAmount() {
      // Replace this constant with a calculation of the order's amount
      // Calculate the order total on the server to prevent
      // people from directly manipulating the amount on the client
      $cart = Session::get('cart');
      return $cart->totalPrice;
    }
    /**
     * handling payment with POST
     */
    public function handlePost(Request $request)
    {
       $cart = Session::get('cart');
      //  $cart = JSON.stringify({amount: $cart->totalPrice});
      //  dd($cart->totalPrice);
        \Stripe\Stripe::setApiKey('sk_test_51Iw57eK9raLUvqgLRbS7LvlERom6JJQgyafZqyGzkamOMRZMoUjkc7spX2P0Sf6mbu7ZqIryASYAMPbRwtEsuFIW00DGQtkSwe');
        header('Content-Type: application/json');
        $YOUR_DOMAIN = 'http://localhost:4242';
        $amount = (int) str_replace('.', '', $request->get('total'));
        $checkout_session = \Stripe\Checkout\Session::create([
          'payment_method_types' => ['card'],
          // dd($checkout_session),
          'line_items' => [[
            'price_data' => [
              'currency' => 'eur',
              'unit_amount' => $amount * 100,
              'product_data' => [
                'name' => 'Stubborn Attachments',
                'images' => ["https://i.imgur.com/EHyR2nP.png"],
              ],
            ],
            'quantity' => 1,
          ]],
          'mode' => 'payment',
          'success_url' => $YOUR_DOMAIN . '/success.html',
          'cancel_url' => $YOUR_DOMAIN . '/cancel.html',
          'metadata' => ['integration_check' => 'accept_a_payment']
        ]);
        // echo json_encode(['id' => $checkout_session->id]);
        return response()->json(['secret' => $checkout_session->id]); 
    }



    // public function stripeCheckout(Request $request)

    // {
    //     $secret_key = env('STRIPE_SECRET');
    //     \Stripe\Stripe::setApiKey($secret_key);
        
    //     $amount = (int) str_replace('.', '', $request->get('total'));
        
    //     $intent = \Stripe\PaymentIntent::create([
    //             'amount' => $amount * 100,
    //             'currency' => 'eur',
    //             'metadata' => ['integration_check' => 'accept_a_payment']
    //         ]);

            
    //     return response()->json(['secret' => $intent->client_secret]);    
    // }
    
}