<x-layout>
    <x-navbar />

    <div class="container">
        <div class="row">
            <h2 class="mt-5 text-center tc-sec">{{ $category }}</h2>
        </div>
        <div class="row border-custom2">
            <div class="col-12 ">
                <a href="{{ route('home') }}"><i class="fas fa-chevron-left tc-accent fs-2"></i></a>
            </div>
            @if (count($articles) == 0)
                <div class="text-center">
                    <p class="tc-accent fs-3 m-3">Non ci sono annunci in questa categoria!</p>
                    <img id="empty" class="img-fluid rounded-circle shadow bg-base w-25 m-2 border-custom2" src="/img/accusing.png"
                        alt="Non ci sono annunci">
                    <p class="tc-accent fs-5"></p>
                    <a class="btn btn-custom3 m-3" href="{{ route('article.create') }}">Inserisci un nuovo annuncio</a>
                </div>
            @endif
            @foreach ($articles as $article)
            <div class="col-12 col-md-4 p-3 mb-4">
                <div class="card rounded mt-2">
                        @if ($article->articleImages()->exists())
                        <img src="{{ asset($article->articleImages()->get()->first()->image_path) }}"
                        class="card-img-top img-fluid rounded w-auto" style="height: 300px">
                        
                        @else
                            <figure class="d-flex justify-content-center align-items-center">
                                <img src="https://via.placeholder.com/150" class="card-img-top img-fluid"
                                    alt="placeholder">
                            </figure>
                        @endif
                        <div class="card-body text-center d-flex flex-column justify-content-between">
                            <div>
                                <h5 class="card-title tc-sec">{{ $article->title }}</h5>
                                <p class="card-text tc-sec">{{ $article->price }} €</p>
                                <p class="card-text tc-sec">{{ $article->category->name }} </p>
                                <p class="card-text tc-sec text-truncate">{{ $article->description }}</p>
                            </div>
                            <div>
                                <a href="{{ route('article.show', compact('article')) }}"
                                    class="btn btn-custom2 underline-cstm">
                                    Dettagli
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>





    <x-footer />

</x-layout>
