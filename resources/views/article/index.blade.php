<x-layout>
    <x-navbar />
        @if (session('status'))
            <div class="alert bg-accent2 tc-white">
                {{ session('status') }}
            </div>
        @endif

        <div class="container">
            <div class="row mt-3 mb-5 border-custom2">
                <div class="col-12 text-center tc-accent my-2 ">
                    <h1 class="">Tutti gli annunci</h1>
                </div>
                
                    @foreach ($articles as $article)
                        <div class="col-12 col-md-4 p-3 mb-4">
                            <div class="card rounded mt-2">
                                @if ($article->articleImages()->exists())
                                <img src="{{ asset($article->articleImages()->get()->first()->image_path) }}"
                                class="card-img-top img-fluid rounded w-auto" style="height: 300px">
                                
                                @else
                                    <figure class="d-flex justify-content-center align-items-center">
                                        <img src="https://via.placeholder.com/150" class="card-img-top img-fluid"
                                            alt="placeholder">
                                    </figure>
                                @endif
                                <div class="card-body text-center">
                                    <h5 class="card-title tc-sec">{{ $article->title }}</h5>
                                    <p class="card-text tc-sec">{{ $article->price }} €</p>
                                    <p class="card-text tc-sec">{{ $article->category->name }} €</p>
                                    <p class="card-text tc-sec text-truncate">{{ $article->description }}</p>
                                    <div class="d-flex flex-wrap justify-content-center justify-content-sm-between">
                                    <a href="{{ route('article.show', compact('article')) }}"
                                        class="btn btn-custom2 underline-cstm my-1">Dettagli</a>
                                    <a href="{{route('article.addToCart', ['id' => $article->id])}}"
                                        class="btn btn-custom underline-cstm my-1 shadow">Add to cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            
        </div>

    <x-footer />
</x-layout>
