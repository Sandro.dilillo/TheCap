<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\ArticleController;
// use GuzzleHttp\Middleware;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('/article/category/{cat}', [PublicController::class, 'category'])->name('category');
Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name('locale');

// contatti pubblic
Route::get('/richiesta_info/{article}', [PublicController::class, 'richiesta_info'])->name('richiesta_info');
Route::get('/richiesta_info/', [PublicController::class, 'richiesta_info2'])->name('richiesta_info.home');
Route::post('/richiesta_info/submit/', [PublicController::class, 'submit'])->name('info.submit');


//search
Route::get('/search', [PublicController::class, 'search'])->name('search');


///////////////////// articleee ///////////////////////


Route::get('/cappelli/index', [ArticleController::class, 'index'])->name('article.index');
Route::get('/cappelli/create', [ArticleController::class, 'create'])->name('article.create');
Route::post('/cappelli/store', [ArticleController::class, 'store'])->name('article.store');
Route::get('/cappelli/show/{article}', [ArticleController::class, 'show'])->name('article.show');
Route::get('/article/edit/{article}', [ArticleController::class, 'edit'])->name('article.edit');
Route::put('/article/update/{article}', [ArticleController::class, 'update'])->name('article.update');
Route::delete('/article/destroy/{article}', [ArticleController::class, 'destroy'])->name('article.destroy');

Route::delete('/article/removeImage/{articleImage}', [ArticleController::class, 'removeImage'])->name('article.removeImage');

////////////////// cart //////////////////////////////

Route::get('/add_to_cart/{id}', [ArticleController::class, 'addToCart'])->name('article.addToCart');
Route::get('/shopping-cart', [ArticleController::class, 'cart'])->name('article.shoppingCart');
Route::put('/cart/update/{id}', [ArticleController::class, 'updateCart'])->name('article.updateCart');
Route::delete('/cart/delete_one_cart/{id}', [ArticleController::class, 'deleteOneCart'])->name('article.deleteOneCart');
Route::delete('/cart/remove_from_cart/{id}', [ArticleController::class, 'removeFromCart'])->name('article.removeFromCart');
Route::get('/shopping-cart/checkout', [ArticleController::class, 'checkout'])->name('article.shoppingCart.checkout');

// Route::post('/cappelli/store', [ArticleController::class, 'store'])->name('article.store');

// ///////////////  stripe ////////////////////////

// Route::get('/stripe-payment', [StripeController::class, 'handleGet']);
Route::post('/stripe-payment', [StripeController::class, 'handlePost'])->name('stripe.payment');

// ///////////////// user //////////////////////

Route::get('/user/profile', [UserController::class, 'profile'])->name('user.profile');
