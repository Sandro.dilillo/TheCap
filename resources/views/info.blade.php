<x-layout>
    <x-navbar />
    <div class="container mt-3" >
        <div class="row justify-content-center border-custom2">
            <div class="col-12 tc-sec text-center my-3">
                <h2>
                    Ti interessa un articolo in particolare?
                </h2>
            </div>
            <div class="col-12 col-md-4 d-flex justify-content-center mb-3">
                <div>
                <img src="/img/agoematassa.png" class="img-fluid rounded-circle bg-base shadow border-custom3 m-0 me-md-4" alt="ago e matassa">
            </div>
        </div>
            <div class="col-12 col-md-4">
                <form method="POST" action="{{route('info.submit')}}" class="text-center tc-accent2">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Come ti chiami?</label>
                        <input name="user" type="text" class="form-control rounded-pill" id="name">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Inserisci qui la tua mail</label>
                        <input name="email" type="email" class="form-control rounded-pill" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <label for="description" class="form-label">Ti piace uno dei nostri prodotti?</label>
                        <textarea name="message" class="form-control rounded" id="description" cols="30" rows="5"></textarea>
                    </div>
              
                    <button type="submit" class="btn btn-custom2 underline-cstm my-3">Invia</button>
                </form>
            </div>
        </div>
    </div>
<x-footer />
</x-layout>
