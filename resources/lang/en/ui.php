<?php

return[

    //home footer e navbar
    'welcome'=> 'Welcome',
    'i nostri cappelli'=>'Our hats',
    'annunci'=>'Releases!',
    'sei nuovo qui'=>'we have already met?',
    'nuove creazioni' => 'New creations',
    'bottone email'=>'Enter',
    'titolo footer'=>'FOLLOW THE RABBIT IN HIS LAIR!',
    'descrizione footer'=>'In the rabbit hole you will find many other users, the goal of our site is to connect the whole world and make your adventure as fast as the white rabbit!',
    'contatti'=>'Contacts',
    'chi siamo'=>'About us',
    'aiuto'=>'Help',
    'vendi'=>'Sell now',
    'dettaglio'=>'Details',
    'segui'=>'follow the rabbit',
    'search'=>'Search for items...',
    'cestino'=>'Trash',
    'ciao'=>'Hello',
    'Accedi||iscriviti'=>'Login || Register',
    'Accedi'=>'Login',
    'registrati'=>'Register',
    'logout'=>'Logout',
    'revisore'=>'Revisor',
    'Le nostre'=>'All',
    'categorie'=>'Categories',
    'cappelli da adulti' => 'Adult hats',
    'cappelli da bimbi' => 'Children\'s hats',
    'vai'=>'Go',
    'conferma'=>'Confirm',


    
    //modale
    'Accedi'=>'Login',
    'registrati'=>'Register',
    'nome utente'=>'User Name',
    'Indirizzo Email'=>'Email Address',
    'non sei ancora'=>"aren't you register yet?",
    'nome e cognome'=>'Name and Surname',
    'iscriviti'=>'Subscribe',

    //messaggio
    'creato con succ'=>'successfully created',
    'solo rev'=>'Access denied, only for revisors!',

    //article/create
    'inserisci'=>'Create',
    'il tuo annuncio'=>'Article',
    'nome articolo'=>'Article name',
    'scegli'=>'Categories',
    'descrizione'=>'Description',
    'prezzo'=>'Price',
    'contatto'=>'Contact',
    'inserisci-i'=>'Image',
    'aggiungi articolo'=>'Create',

    //dettaglio
    'creato il'=>'Created on',
    'da'=>'from',
    'contattami qui'=>'contact me :',
    'torna alla home'=>'Go home',

    //Revisor home
    'titolo'=>'There are no Ads to review',
    'sotto-titolo'=>'Coffee time bro!',
    'annuncio nr'=>'Article Nr',

    //revisor trash
    'titolo-trash'=>'There are no ads in your trash',
    'sotto-trash'=>'Too much coffee is bad!',
    'cancella'=>'Delete',


    //lavora con noi
    'lavora con'=>'Works whit',
    'noi!'=>'Us!',

];
